import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewDashboardComponent } from "./new-dashboard/new-dashboard.component";
import { NewVideoRoomComponent } from "./new-video-room/new-video-room.component";

const routes: Routes = [
  { path: '', component: NewDashboardComponent },
  { path: ':roomName', component: NewVideoRoomComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
