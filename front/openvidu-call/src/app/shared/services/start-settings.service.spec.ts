import { TestBed } from '@angular/core/testing';

import { StartSettingsService } from './start-settings.service';

describe('StartSettingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StartSettingsService = TestBed.get(StartSettingsService);
    expect(service).toBeTruthy();
  });
});
