import { Injectable } from '@angular/core';
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StatesService {
  public settingsScreenClicked = new Subject<any>();

  constructor() { }
}
