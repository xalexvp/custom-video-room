import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/internal/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { StartSettings } from "../models/start-settings";

@Injectable({
  providedIn: 'root'
})
export class StartSettingsService {

  firstCall: boolean = true;

  startSettings: StartSettings = {
    "user": {
      "name": "Student"
    },
    "room": {
      "name": "Lesson"
    }
  };

  constructor( private http: HttpClient ) { }

  getData() {

    const returnRequest = this.http.get('assets/user_config.json');

    return returnRequest

      .pipe(
        catchError((err: HttpErrorResponse) => {
          return throwError(this.startSettings);
        }),
        map((result: StartSettings) => {
          if (result && result.room) {
            this.startSettings = result;
          }
          return result;
        })
      );
  }
}


