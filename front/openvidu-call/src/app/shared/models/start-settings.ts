export interface StartSettings {
  "user": {
    "name": string
  },
  "room": {
    "name": string
  }
}
