import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StartSettingsService } from "../shared/services/start-settings.service";
import {StartSettings} from "../shared/models/start-settings";

@Component({
  selector: 'app-new-dashboard',
  templateUrl: './new-dashboard.component.html',
  styleUrls: ['./new-dashboard.component.scss']
})
export class NewDashboardComponent implements OnInit {
  public version = require('../../../package.json').version;
  public roomName: string;
  public userName: string;
  showForm: boolean;

  constructor(
    private router: Router,
    private startSettingsService: StartSettingsService ) {}

  ngOnInit() {
    if (this.startSettingsService.firstCall) {
      this.initData();
    } else {
      this.showForm = true;
    }
  }

  initData() {
    this.startSettingsService.getData()
      .subscribe((settings: StartSettings) => {
        this.redirectScreen(settings);
      }, (data) => {
        this.redirectScreen(data);
      });
  }

  redirectScreen(data) {
    if (data.room) {
      this.startSettingsService.firstCall = false;
      this.roomName = data.room.name;
      this.userName = data.user.name;
      this.goToVideoCall();
    }
  }

  public restoreVideoCall() {
    if (this.roomName) {
      this.goToVideoCall();
    } else if (!this.startSettingsService.firstCall) {
      this.roomName = this.startSettingsService.startSettings.room.name;
      this.goToVideoCall();
    } else {
      this.initData();
    }
  }
  goToVideoCall () {
    this.router.navigate(['/', this.roomName]);
  }
}

