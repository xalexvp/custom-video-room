import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVideoRoomComponent } from './new-video-room.component';

describe('NewVideoRoomComponent', () => {
  let component: NewVideoRoomComponent;
  let fixture: ComponentFixture<NewVideoRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVideoRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVideoRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
